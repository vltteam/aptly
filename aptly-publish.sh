#!/bin/bash
# Usage: aptly-publish.sh http://repo.url prefix xenial main mypackage-1.0.deb s3:endpoint
set -euxo pipefail

# Required input parameters
export REPO_URL=$1
export PREFIX=$2
export DISTR=$3
export COMP=$4
export PKG=$5
export S3="$6:$PREFIX"

# Internal use only vars
export REPO=dummy
export SNAPSHOT=local-$(date --iso)
export MIRROR_SNAPSHOT=mirror-snapshot-$(date --iso)
export MERGE_SNAPSHOT=merge-snapshot-$(date --iso)

# Create local repo and add new package to it
aptly repo create -comment="" -distribution=$DISTR $REPO &&
aptly repo add $REPO $(realpath $PKG) &&
aptly snapshot create $SNAPSHOT from repo $REPO

# Mirror, snapshot and merge the existing remote repo with the local snapshot
wget -qO- $REPO_URL/Release.key | gpg --no-default-keyring --keyring trustedkeys.gpg --import || true
aptly mirror create $REPO $REPO_URL/$PREFIX $DISTR &&
aptly mirror update $REPO &&
aptly snapshot create $MIRROR_SNAPSHOT from mirror $REPO &&
aptly snapshot merge -latest $MERGE_SNAPSHOT $SNAPSHOT $MIRROR_SNAPSHOT || MERGE_SNAPSHOT=$SNAPSHOT

# Publish merged snapshot & switch to remove obsoleted files
aptly publish snapshot -batch -distribution=$DISTR -passphrase="$PRIVATE_KEY_PASS" $MERGE_SNAPSHOT $S3 &&
aptly publish switch -batch -passphrase="$PRIVATE_KEY_PASS" $DISTR $S3 $MERGE_SNAPSHOT
