## [Aptly](https://www.aptly.info/) docker image

Based on Ubuntu image with added Aptly packages.

### Creating new repository

#### Generate keys

```bash
mkdir -p ~/.gnupg
printf "cert-digest-algo SHA256\ndigest-algo SHA256\n" >> ~/.gnupg/gpg.conf
gpg --gen-key
gpg --export --armor key@example.com > public.gpg
gpg --export-secret-key -a "<username>" > private.gpg
```
